import { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import { State } from './types'

export const useIsLoading = () => {
  return useSelector((state: any) => {

    return state.app.is_loading || false
  })
}
