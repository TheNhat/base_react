import userActions from './userActions'
import appActions from './appActions'

const rootAction = {
  userActions,
  appActions
}

export default rootAction
