// import * as types from "../constants/index"

import types from "./types"

const addIsLoading = (value: any) => {
  return {
      type: types.LOADING,
      payload: value,
  };
};

export default {
  addIsLoading
}