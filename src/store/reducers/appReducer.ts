import types from '../actions/types';

const defaultState = {
  isOpen: false,
  content: '',
  title: '',
  type: '',
  dataConfirm: {},
};

export default (state = defaultState, action:any) => {
  const type = action?.type
  switch (action.type) {
    case types.LOADING:
      return {
        ...state,
        is_loading: action.payload
      };
    default:
      return state;
  }
};
