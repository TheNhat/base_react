import { useDispatch, useSelector } from 'react-redux'
import rootAction from 'store/actions';

// const setStateLoading = (loading: boolean) => {
//   const dispatch = useDispatch();

//   dispatch(rootAction.userActions.addIsLoading({ is_loading: loading }));				
//   return true
// }
// export default setStateLoading;

export const setStateLoading = (loading: boolean) => async (dispatch?: any) => {
    
  dispatch(rootAction.appActions.addIsLoading({ is_loading: loading }));				
}; 
