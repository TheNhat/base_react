// import { Toast } from '@gobitbunnyswap/uikit'
import BigNumber from 'bignumber.js'

export type TranslatableText =
  | string
  | {
      id: number
      fallback: string
      data?: {
        [key: string]: string | number
      }
    }

// Global state

export interface State {
  account: AccountState,
  indicator: IndicatorState,
  userInfo: UserInfoState,
}

export interface AccountState {
  data?: any
}

export interface IndicatorState {
  isShow?: any
}

export interface UserInfoState {
  address?: string
  email?: string
  userRole?: string
  id?: any
  tier?: any
  userId?: any
  nickName?: any
  avatarUrl?: any,
  ranking?: number
  baseAvatar1?: any
  baseAvatar2?: any
  baseAvatar3?: any
  currentDepositGold: string,
  seasonId?:number,
  userName?:string
}
