import Loading from 'containers/Loading';
import React, {Suspense} from 'react';
import ReactDOM from 'react-dom';
import {Provider as StoreProvider} from 'react-redux';
import initStore from 'store';
import App from './App';
import * as serviceWorker from './serviceWorker';
const store = initStore();

ReactDOM.render(
  <React.StrictMode>
  <StoreProvider store={store}>
  <Suspense fallback={<Loading />}>
    <App />
    </Suspense>
    </StoreProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
