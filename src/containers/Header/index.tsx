import { Button, Input } from 'components';
import React from 'react';//lazy
import * as S from './style';
interface Props {
}

function Header({}: Props): React.ReactElement {

	return (
		<S.Wrapper>
			<Button children={'Button Test'}/>
			<Input/>
		</S.Wrapper >
	);
}

export default Header;