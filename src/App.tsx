import React, { useEffect, lazy } from 'react';

import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from 'react-router-dom';
import { ThemeProvider } from 'styled-components';
import defaultTheme from 'common/style/themes/default';
import Normalize from 'common/style/Normalize';
import GlobalStyles from 'common/style/GlobalStyle';
const Home = lazy(() => import("pages/Home"))

const App: React.FC = () => {
  return (
    <ThemeProvider theme={defaultTheme}>
      <Normalize />
      <GlobalStyles />
      <Router>
        <Switch>
          <PublicRoute exact path="/" component={Home} />
        </Switch>
      </Router>
    </ThemeProvider>
  );
};

export default App;

function PublicRoute({ component: TargetPage, ...rest }: any): React.ReactElement {
  // console.log(rest, TargetPage);
  return <Route
    {...rest}
    render={(props) => !localStorage.getItem("token") ? <TargetPage {...props} /> : <Redirect to={{ pathname: '/' }} />}
  />
}


function PrivateRoute({ component: TargetPage, isAuthenticated, ...rest }: any): React.ReactElement {
  // console.log(TargetPage, 'TargetPage');
  // console.log(isAuthenticated, 'isAuthenticated');
  return <Route
    {...rest}
    render={(props) => localStorage.getItem("token") ? <TargetPage {...props} /> : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />}
  />
}
