const NotoSansKrBlack = require('./NotoSansKR-Black.otf');
const NotoSansKrBold = require('./NotoSansKR-Bold.otf');
const NotoSansKrLight = require('./NotoSansKR-Light.otf');
const NotoSansKrMedium = require('./NotoSansKR-Medium.otf');
const NotoSansKrRegular = require('./NotoSansKR-Regular.otf');
const NotoSansKrThin = require('./NotoSansKR-Thin.otf');

export {
    NotoSansKrBlack,
    NotoSansKrBold,
    NotoSansKrLight,
    NotoSansKrMedium,
    NotoSansKrRegular,
    NotoSansKrThin
}