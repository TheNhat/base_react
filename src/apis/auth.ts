import { mainAxios } from "libs/axios";

  export const registerApi = (
    payload : any
  ) => {
    return mainAxios.request({
      methodType: 'POST',
      url: `/api/general/auth/register`,
      payload: payload,
      config: {
        headers: {
          contentType: 'application/json'
        }
      }
    })
  };