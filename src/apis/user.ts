import { mainAxios } from "libs/axios";

export const getCurrentUser = () => {
  return mainAxios.getRequest({
    url: `/api/general/user/current`,
    requiresToken: true
  })
};