import { createGlobalStyle } from 'styled-components';
import { theme } from 'styled-tools';
import {
  NotoSansKrBlack,
  NotoSansKrBold,
  NotoSansKrLight,
  NotoSansKrMedium,
  NotoSansKrRegular,
  NotoSansKrThin
} from 'assets/font/fonts';

const GlobalStyles = createGlobalStyle`


  @font-face {
    font-family: 'NotoSansKR-Thin';
    src: local('NotoSansKR-Thin'), url(${NotoSansKrThin}) format('opentype');
}

@font-face {
    font-family: 'NotoSansKR-Light';
    src: local('NotoSansKR-Light'), url(${NotoSansKrLight}) format('opentype');
}

@font-face {
    font-family: 'NotoSansKR-Regular';
    src: local('NotoSansKR-Regular'), url(${NotoSansKrRegular}) format('opentype');
}

@font-face {
    font-family: 'NotoSansKR-Medium';
    src: local('NotoSansKR-Medium'), url(${NotoSansKrMedium}) format('opentype');
}

@font-face {
    font-family: 'NotoSansKR-Bold';
    src: local('NotoSansKR-Bold'), url(${NotoSansKrBold}) format('opentype');
}

@font-face {
    font-family: 'NotoSansKR-Black';
    src: local('NotoSansKR-Black'), url(${NotoSansKrBlack}) format('opentype');
}

  * {
    margin: 0;
    box-sizing: border-box;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
  *,
  *::before,
  *::after {
    box-sizing: border-box;
  }
  html {
    font-size: 14px;
  }
  body {
    height: 100%;
  }
  a, button {
    text-decoration: none;
    cursor: pointer;
  }

  h1 {
    ${theme('fontStyle.h3')}
  }

  h2 {
    ${theme('fontStyle.h4')}
  }

  h2 {
    ${theme('fontStyle.h5')}
  }

  h3 {
    ${theme('fontStyle.subtitle1')}
  }

  h4 {
    ${theme('fontStyle.subtitle2')}
  }

  h5 {
    ${theme('fontStyle.body1')}
  }

  text{
    font-family: 'NotoSansKR-Bold';
  }

  p {
    ${theme('fontStyle.body2')}
  }
  .hljs{
    background: #000000;
  }
  -ms-overflow-style: none;  /* IE and Edge */
  scrollbar-width: none;
  ::-webkit-scrollbar {
    display: none;
  }
  outline : none;

  .btn-default{
    font-family: NotoSansKR-Bold;
    font-size: 16px;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.5;
    letter-spacing: normal;
    text-align: center;
    color: #ffffff;
    text-transform: uppercase;
    border-radius: 25px;
    background-image: linear-gradient(to right, #e88220, #e15930);
    border: none;
    height: 50px;
    width: 100%;
  }

  .btn-cancel{
    font-family: NotoSansKR-Bold;
    font-size: 16px;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.5;
    letter-spacing: normal;
    text-align: center;
    color: #ffffff;
    text-transform: uppercase;
    border-radius: 25px;
    background: #242424;
    border: none;
    height: 50px;
    width: 100%;
  }
`;

export default GlobalStyles;

