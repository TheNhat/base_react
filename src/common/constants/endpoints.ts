export default {
    LOGIN_URL: '/api/v1/login',
    SIGNUP_URL: '/api/v1/signup',
};
