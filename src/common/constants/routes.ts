export default {
  HOME: '/',
  SIGNUP: '/signup',
  LOGIN: '/login',
  FINDID: '/findId',
  LOGOUT: '/logout',
  USER: '/user',
  EVENT_CREATE: '/event/create',
  EVENT_DETAIL: '/events',
  MYPAGE_TICKETS: '/my/tickets',
  MYPAGE_CREATED_EVENTS: '/my/events',
  MYPAGE_TICKETS_EVENT: '/my/tickets/event',
  CONFIRM_FORGOT_PASSWORD:'/forgotpassword/confirm'
};
