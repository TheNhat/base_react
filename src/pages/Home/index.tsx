import { Header } from 'containers';
import React from 'react';//lazy
import * as S from './style';
interface Props {
}

function HomePage({}: Props): React.ReactElement {

	return (
		<S.HomeWrapper>
			<Header/>
		</S.HomeWrapper >
	);
}

export default HomePage;
