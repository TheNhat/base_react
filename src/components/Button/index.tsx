import React from 'react';//lazy
import * as S from './style';
import { Button } from 'antd';

interface Props {
	children?: any;
}

function ButtonComponent({ children }: Props): React.ReactElement {

	return (
		<Button type="primary">
			{children}
		</Button>
	);
}

export default ButtonComponent;
