import React from 'react';//lazy
import * as S from './style';
import { Input } from 'antd';

interface Props {
	children?: any;
}

function InputComponent({ children }: Props): React.ReactElement {

	return (
		<S.InputStyle placeholder="Basic usage" />
	);
}

export default InputComponent;
