import * as path from 'path';

module.exports = {
    entry: [
        require.resolve('./polyfills'),
        require.resolve('react-dev-utils/webpackHotDevClient'),
        path.appIndexJs
    ],
    resolve: {
        alias: {
            '@': path.join(__dirname, '../src') // <--- Here
        }
    },
    test: /\.svg$/,
    use: [
        {
            loader: 'babel-loader',
            options: {
                presets: ['preact', 'env'],
            },
        },
        {
            loader: '@svgr/webpack',
            options: { babel: false },
        }
    ],
}